package com.aliware.edas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SentinelDegradeExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(SentinelDegradeExampleApplication.class, args);
	}

}
