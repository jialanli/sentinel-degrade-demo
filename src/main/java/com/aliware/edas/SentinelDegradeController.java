package com.aliware.edas;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SentinelDegradeController {
	
    @GetMapping("/rt")
    public String degradeRt() {
        try {
        	System.out.println("********");
            Thread.sleep(2000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "rt success";
    }

    @GetMapping("/exception")
    public String degradeException() {
    	System.out.println("********");
        throw new RuntimeException("custom exception");
    }
}
